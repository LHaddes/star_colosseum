﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arme : MonoBehaviour
{
    public Camera camera;
    public Rigidbody2D rbArme;
    public Transform firePointTransform;
    
    private Vector2 mousePosition;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Récupère la position de la sourie
        mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
        
    }

    void FixedUpdate()
    {
        // Calcule de la direction en fonction de la position de la sourie et de l'arme
        Vector2 lookDirection = mousePosition - rbArme.position;
        // Calcule de l'angle de l'arme
        float angleArme = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        // La rotation change de valeur en fonction de l'angle de l'arme
        rbArme.rotation = angleArme;
        FlipArm(Vector2.SignedAngle(transform.right, Vector2.right));
    }
    
    void FlipArm(float rotation)
    {
        if (rotation < -90f || rotation > 90f)
        {
            spriteRenderer.flipY = true;
            FlipFirePoint(true);
        }
        else
        {
            spriteRenderer.flipY = false;
            FlipFirePoint(false);
        }
    }
    
    void FlipFirePoint(bool flip)
    {
        var pos = firePointTransform.localPosition;
        pos.y = Mathf.Abs(pos.y) * (flip ? -1 : 1);
        firePointTransform.localPosition = pos;
    }
}
