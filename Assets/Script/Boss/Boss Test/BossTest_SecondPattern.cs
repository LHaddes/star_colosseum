﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTest_SecondPattern : State
{
    private float countdown;
    public BossTest_SecondPattern(Boss boss) : base(boss)
    {
        
    }

    public override void OnStateEnter()
    {
        Debug.Log("Second patterne");
    }

    public override void Tick()
    {
        countdown += Time.deltaTime;
        if (countdown >= 2f)
        {
            boss.SetState(new BossTest_Idle(boss));
        }
    }

    public override void OnStateExit()
    {
    }
}
