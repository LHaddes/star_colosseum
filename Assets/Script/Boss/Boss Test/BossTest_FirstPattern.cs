﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTest_FirstPattern : State
{
    private float countdown;
    public BossTest_FirstPattern(Boss boss) : base(boss)
    {
    }

    public override void OnStateEnter()
    {
        Debug.Log("First pattern");
    }

    public override void Tick()
    {
        countdown += Time.deltaTime;
        if (countdown >= 2f)
        {
            boss.SetState(new BossTest_Idle(boss));
        }
    }

    public override void OnStateExit()
    {
    }
}
