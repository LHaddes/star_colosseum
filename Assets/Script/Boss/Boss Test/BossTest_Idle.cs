﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTest_Idle : State
{
    private float countdown;
    public BossTest_Idle(Boss boss) : base(boss)
    {
        
    }
    public override void OnStateEnter()
    {
       
    }

    public override void Tick()
    {
        countdown += Time.deltaTime;
        if (boss.counter < 4 || boss.counter > 4 && boss.counter < 8 || boss.counter > 8)
        {
            if (countdown >= 1.5f)
            {
                boss.SetState(new BossTest_FirstPattern(boss));
            }

        }
        else if (boss.counter == 4)
        {
            boss.SetState(new BossTest_SecondPattern(boss));
        }
        else if (boss.counter == 8)
        {
            boss.SetState(new BossTest_SecondPattern(boss));
        }
    }

    public override void OnStateExit()
    {
        boss.counter++;
    }
}
