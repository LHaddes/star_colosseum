﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected Boss boss;

    public abstract void Tick();

    public virtual void OnStateEnter() { }

    public virtual void OnStateExit(){ }

    public State(Boss boss)
    {
        this.boss = boss;
    }
}
