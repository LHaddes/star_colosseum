﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouvement : MonoBehaviour
{
    private Rigidbody2D rb;
    private Camera cam;
    public float dashTimer, dashTimerLimit;
    public float moveSpeed, maxSpeed;
    public float dashForce;

    private Vector2 _movement, _mousePos;

    public bool doDash, _playerDash;
    

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = Camera.main;    
    }

    void Update()
    {
        _movement.x = Input.GetAxisRaw("Horizontal");
        _movement.y = Input.GetAxisRaw("Vertical");

        _mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        
        rb.MovePosition(rb.position + _movement * moveSpeed * Time.fixedDeltaTime);
        
        Vector2 lookDir = _mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;

        
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("sf");
            rb.MovePosition(rb.position + _movement * dashForce * Time.fixedDeltaTime);
        }
    }

   

    
    /*public void Move()
        {
            if (doDash && !_playerDash)
            {
                _defaultSpeed = PlayerStats.Instance.speed;
                PlayerStats.Instance.speed *= 3;
                _playerDash = true;
            }
            rb.AddForce(movement * PlayerStats.Instance.speed, ForceMode2D.Impulse);
    
            if (_playerDash)
            {
                dashTimer += Time.deltaTime;
                rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed * 20);
                if (dashTimer > dashTimerLimit)
                {
                    _playerDash = false;
                    doDash = false;
                    dashTimer = 0;
                    rb.velocity = Vector2.zero;
                    PlayerStats.Instance.speed = _defaultSpeed;
                }
            }
            else
            {
                rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);
            }
            if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
            {
                rb.velocity *= 0.5f;
            }
    
            PlayerStats.Instance.VariableTest();
        }*/
}
