﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePointTransform;
    public GameObject bulletPrefab;
    public float bulletForce = 10f;
    
    private float fireRate = 0.5f;
    private float nextFire = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }
        
        
    }

    void Shoot()
    {
        if (Time.time >= fireRate)
        {
            // Créé la bullet
            GameObject bullet = Instantiate(bulletPrefab, firePointTransform.position, firePointTransform.rotation);
            // Récupération du RigidBody2D de la bullet
            Rigidbody2D rbBullet = bullet.GetComponent<Rigidbody2D>();
            // Ajout de force
            rbBullet.AddForce(firePointTransform.right * bulletForce, ForceMode2D.Impulse);
        }
        
    }
}
